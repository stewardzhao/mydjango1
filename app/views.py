from django.http import HttpResponse, request
from django.shortcuts import render

from app.models import Student


def addstudent(request):
    student = Student()
    student.s_name = 'Jack12'
    student.save()
    context = {
        's_name': student.s_name,
    }
    # return HttpResponse("Hello")
    return render(request, 'addstudent.html', context=context)


def have_request(request):
    requests = request.META

    return render(request,'requestMeta.html', context=locals())